---
title: Whatsapp
subtitle: Instant Messengers
provider: facebook
order:
    - signal
    - matrix
    - xmpp
aliases:
    - /ethical-alternatives-to-whatsapp-and-skype/
---